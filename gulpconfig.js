var paths = {};

paths.basedir = global.process.env.INIT_CWD;
paths.gulp_module = paths.basedir + '/node_modules/gulp-base';
paths.gulp_node_modules = paths.gulp_module + '/node_modules';
paths.tasks = paths.gulp_module + '/tasks';
paths.public = paths.basedir + '/public';
paths.views = paths.public + '/views';

paths.src = {
  styles  : paths.public + '/src/styles',
  scripts : paths.public + '/src/scripts',
  images  : paths.public + '/src/images',
  vendor  : paths.public + '/src/components'
};

paths.dist = {
  styles  : paths.public + '/dist/assets/css',
  scripts : paths.public + '/dist/assets/js',
  images  : paths.public + '/dist/assets/img'
};

module.exports = {

  paths: paths,

  templates: {
    config: paths.gulp_module + 'config.js',
  },

  files: {

    jshintrc: paths.basedir + '/.jshintrc',

    src: {
      styles: {
        app: [
          paths.src.styles + '/app.sass'
        ],
        vendor: [
          paths.src.vendor + '/normalize-css/normalize.css',
          paths.src.vendor + '/angular-material/angular-material.css'
        ]
      },
      scripts: {
        app: [
          paths.src.scripts + '/app.js',
          paths.src.scripts + '/filters.js',
          paths.src.scripts + '/controllers/**/*.js',
          paths.src.scripts + '/directives/**/*.js',
          paths.src.scripts + '/services/**/*.js'
        ],
        vendor: [
          paths.src.vendor + '/angular/angular.js',
          paths.src.vendor + '/angular-animate/angular-animate.js',
          paths.src.vendor + '/angular-aria/angular-aria.js',
          paths.src.vendor + '/angular-material/angular-material.js',
          paths.src.vendor + '/modernizr/modernizr.js'
        ]
      }
    },

    dist: {
      styles: {
        app: 'app.css',
        vendor: 'vendor.css'
      },
      scripts: {
        app: 'app.js',
        vendor: 'vendor.js'
      }
    }

  },

  imagemin: {
    optimizationLevel: 5,
    progressive: true,
    interlaced: true
  },

  minifyCss: {
    keepSpecialComments: 0
  },

  plumber: {
    errorHandler: function (err) {
      var plugins = require(paths.gulp_node_modules + '/gulp-load-plugins')({
            config: paths.gulp_module + '/package.json'
          }),
          err_message = plugins.util.colors.red(err);
      plugins.util.beep();
      plugins.util.log(err_message);
      this.emit('end');
    }
  },

  rename: {
    suffix: '.min'
  },

  sass: {
    style: 'expanded',
    includePaths: paths.src.vendor,
    indentedSyntax: true
  },

  sourcemaps: {
    sourceRoot: 'source-maps'
  }

};
