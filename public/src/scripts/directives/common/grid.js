(function () { 'use strict';

  var directives = angular.module('AngularBaseApp.directives', []);

  directives.directive('grid', function() {
    return {
      restrict: 'E',
      templateUrl: 'views/common/grid.html'
    };
  });

}());
